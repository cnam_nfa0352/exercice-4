package fr.cnam.foad.nfa035.fileutils.badgewallet;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageByteArrayFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BadgeWalletDAO {

    private String csvPathFile;
    private List<File> imageFiles;

    public BadgeWalletDAO(final String csvPathFile) {
        this.csvPathFile = csvPathFile;
        this.imageFiles = new ArrayList<>();
    }

    public String getCsvPathFile() {
        return csvPathFile;
    }

    public void setCsvPathFile(final String csvPathFile) {
        this.csvPathFile = csvPathFile;
    }

    public List<File> getImageFiles() {
        return imageFiles;
    }

    public void setImageFiles(final List<File> imageFiles) {
        this.imageFiles = imageFiles;
    }

    public void addBadge(final File image) throws IOException {
        final ImageByteArrayFrame media = new ImageByteArrayFrame(new ByteArrayOutputStream());
        final ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);

        final String encodedImage = media.getEncodedImageOutput().toString();
        this.writeInCsvFile(encodedImage);
    }

    private void writeInCsvFile(final String encodedImage) {
        final File csvFile = new File(this.getCsvPathFile());

        try (FileWriter outputfile = new FileWriter(csvFile)) {
            outputfile.write(encodedImage);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

